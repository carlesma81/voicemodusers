The web API is built whith .NET Core 3.1. You need to download and install this 
runtime version to run it (or SDK for debug) from this web site:
https://dotnet.microsoft.com/download

To test the app, clone the repository to your local machine and open the solution 
file VoiceModUsers.sln in Visual Studio. Then press de F5 button to run it.

The database file is downloaded with the rest of the project and the connection 
string uses a relative path to it, so you don't need to install it or deploy it.
Its ready to use as soon as dowloaded.

The project redirects the browser to the swagger page too. So you can test all 
the API endpoints from there. The update and delete methods require to be 
authorized by the token returned by the user login. The create user function don't.