﻿using System.ComponentModel.DataAnnotations;

namespace VoiceModUsersAPI.Models
{
    public class UserUpdate : UserData
    {
        [Required(ErrorMessage = "Current password is a required field")]
        [StringLength(50, ErrorMessage = "Current password lenght must be between 6 and 50 characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Current_Password { get; set; }
    }
}
