﻿using System.ComponentModel.DataAnnotations;

namespace VoiceModUsersAPI.Models
{
    public class UserData : UserLogin
    {

        [Required(ErrorMessage = "Name is a required field")]
        [StringLength(100, ErrorMessage = "Name lenght must be between 2 and 100 characters", MinimumLength = 2)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Surnames are required fields")]
        [StringLength(300, ErrorMessage = "Surnames lenght must be between 2 and 300 characters", MinimumLength = 2)]
        public string Surnames { get; set; }

        [Required(ErrorMessage = "Country is a required field")]
        [StringLength(100, ErrorMessage = "Country lenght must be between 2 and 100 characters", MinimumLength = 2)]
        public string Country { get; set; }

        [Required(ErrorMessage = "Phone is a required field")]
        [StringLength(20, ErrorMessage = "Phone lenght must be between 9 and 20 characters", MinimumLength = 9)]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        [DataType(DataType.PostalCode)]
        public int Postal_Code { get; set; }

    }
}
