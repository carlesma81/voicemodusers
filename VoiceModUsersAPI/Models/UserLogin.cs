﻿using System.ComponentModel.DataAnnotations;

namespace VoiceModUsersAPI.Models
{
    public class UserLogin
    {
        [Key]
        [Required(ErrorMessage = "Email is a required field")]
        [StringLength(100, ErrorMessage = "Email lenght must be between 5 and 100 characters", MinimumLength = 5)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "New password is a required field")]
        [StringLength(50, ErrorMessage = "New password lenght must be between 6 and 50 characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string New_Password { get; set; }
    }
}
