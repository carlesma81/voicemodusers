﻿using Microsoft.EntityFrameworkCore;
using VoiceModUsersAPI.Models;

namespace VoiceModUsersAPI.Contexts
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<UserData> UserData { get; set; }

    }
}
