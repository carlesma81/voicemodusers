﻿using System.Threading.Tasks;
using VoiceModUsersAPI.Models;

namespace VoiceModUsersAPI.Services
{
    public interface IUserDataService
    {
        string CreateTokenJWT(UserData userData);

        Task<UserData> GetUserData(string email);

        Task<bool> UpdateUserData(UserData userdata, UserData userdb);

        Task<bool> CreateUserData(UserData userdata);

        Task<bool> DeleteUserData(UserData userdata);
    }
}
