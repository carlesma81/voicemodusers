﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using VoiceModUsersAPI.Contexts;
using VoiceModUsersAPI.Models;

namespace VoiceModUsersAPI.Services
{
    public class UserDataService : IUserDataService
    {
        private readonly AppDbContext _context;

        private readonly IConfiguration configuration;

        public UserDataService(AppDbContext context, IConfiguration configuration)
        {
            this._context = context;
            this.configuration = configuration;
        }

        public string CreateTokenJWT(UserData userData)
        {
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration["JWT:SecretKey"])
                );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var _Header = new JwtHeader(_signingCredentials);

            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, userData.Email),
                new Claim(ClaimTypes.Name, userData.Name),
                new Claim(ClaimTypes.Surname, userData.Surnames),
                new Claim(ClaimTypes.Country, userData.Country)
            };

            var _Payload = new JwtPayload(
                issuer: configuration["JWT:Issuer"],
                audience: configuration["JWT:Audience"],
                claims: _Claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddHours(24)
            );


            var _Token = new JwtSecurityToken(
                _Header,
                _Payload
            );

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }

        public async Task<UserData> GetUserData(string email)
        {
            return await _context.UserData.FindAsync(email);
        }

        public async Task<bool> CreateUserData(UserData userdata)
        {
            _context.UserData.Add(userdata);

            return await _context.SaveChangesAsync() == 1;
        }

        public async Task<bool> UpdateUserData(UserData userdata, UserData userdb)
        {
            _context.Entry(userdb).State = EntityState.Detached;

            _context.Entry(userdata).State = EntityState.Modified;            

            return await _context.SaveChangesAsync() == 1;
        }

        public async Task<bool> DeleteUserData(UserData userdata)
        {
            _context.UserData.Remove(userdata);

            return await _context.SaveChangesAsync() == 1;
        }
    }
}
