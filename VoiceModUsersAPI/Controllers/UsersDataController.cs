﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VoiceModUsersAPI.Models;
using VoiceModUsersAPI.Services;

namespace VoiceModUsersAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersDataController : ControllerBase
    {
        private readonly IUserDataService _userDataService;

        public UsersDataController(IUserDataService userDataService)
        {
            this._userDataService = userDataService;
        }

        // PUT: api/UsersData/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        /// <summary>
        /// User update. Requires been authorized by the token returned by the login.
        /// </summary>
        /// <remarks>
        /// Request sample:
        ///
        ///     PUT
        ///     {
        ///        "Name":"José",
        ///        "Surnames":"Lopez Garcia",
        ///        "Email":"jlopez@mail.com",
        ///        "Current_Password":"Contaseña",
        ///        "New_Password":"Nueva_Contaseña",
        ///        "Country":"España",
        ///        "Phone":"+35 123 456 789",
        ///        "Postal_Code":46000
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Return updated user</response>
        /// <response code="400">Wrong current password</response>
        /// <response code="404">User do not exist</response>
        [HttpPut("{email}")]
        [Authorize]
        public async Task<ActionResult<UserData>> PutUserData(string email, UserUpdate userupdate)
        {

            var userdb = await _userDataService.GetUserData(email);

            if (userdb == null)
            {
                return NotFound();
            }

            if (userupdate.Current_Password != userdb.New_Password)
            {
                return BadRequest("Error: Wrong password.");
            }

            if (email != userupdate.Email)
            {
                return BadRequest("Error: Inconsistent data.");
            }

             var successful = await _userDataService.UpdateUserData(userupdate, userdb);
             if (!successful)
             {
                 return BadRequest("Error: Unknow exception.");
             }

            return Ok(userupdate);
        }

        // POST: api/UsersData
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        /// <summary>
        /// User create.
        /// </summary>
        /// <remarks>
        /// Request sample:
        ///
        ///     POST
        ///     {
        ///        "Name":"José",
        ///        "Surnames":"Lopez Garcia",
        ///        "Email":"jlopez@mail.com",
        ///        "New_Password":"Contaseña",
        ///        "Country":"España",
        ///        "Phone":"+35 123 456 789",
        ///        "Postal_Code":46000
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Return created user</response>
        /// <response code="409">User already exists</response>
        [HttpPost]
        public async Task<ActionResult<UserData>> PostUserData(UserData userdata)
        {
        
            if (await _userDataService.GetUserData(userdata.Email) != null) 
            {
                return StatusCode(StatusCodes.Status409Conflict);
            }

             var successful = await _userDataService.CreateUserData(userdata);
             if (!successful)
             {
                 return BadRequest("Error: Unknow exception.");
             }

            return Ok(userdata);
        }

        // DELETE: api/UsersData/5
        /// <summary>
        /// User delete. Requires been authorized by the token returned by the login.
        /// </summary>
        /// <response code="200">Return deleted user</response>
        /// <response code="404">User do not exist</response>
        [HttpDelete("{email}")]
        [Authorize]
        public async Task<ActionResult<UserData>> DeleteUserData(string email)
        {
            var userdata = await _userDataService.GetUserData(email);
            if (userdata == null)
            {
                return NotFound();
            }

            var successful = await _userDataService.DeleteUserData(userdata);
            if (!successful)
            {
                return BadRequest("Error: Unknow exception.");
            }

            return Ok(userdata);
        }

        // POST: api/Login
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        /// <summary>
        /// User login.
        /// </summary>
        /// <remarks>
        /// Request sample:
        ///
        ///     POST
        ///     {
        ///        "Email":"jlopez@mail.com",
        ///        "New_Password":"Contaseña"
        ///     }
        ///
        /// </remarks>
        /// <response code="200">Return the created token</response>
        /// <response code="401">User do not exists</response>
        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLogin userlogin)
        {
            var userInfo = await _userDataService.GetUserData(userlogin.Email);
            if (userInfo != null && userInfo.New_Password == userlogin.New_Password)
            {
                return Ok(new { token = _userDataService.CreateTokenJWT(userInfo) });
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}